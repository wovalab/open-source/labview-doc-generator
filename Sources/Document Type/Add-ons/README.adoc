This folder is the place for Antidoc Document Type add-ons.

IMPORTANT: Antidoc project must not create any file or folder here.

NOTE: Add-ons are managed in different project and are installed with specific package.