---
title: "New documentation website"
date: 2022-10-24T10:02:10+02:00
tags: ["doc"]
draft: false
---

We worked hard to put together all information needed to use Anidoc and all other Wovalab open source project in the same website.

This website is now available **[here](https://wovalab.gitlab.io/open-source/docs/)**.

**NOTE:** this website will note be updated anymore