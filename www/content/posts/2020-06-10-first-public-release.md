---
title: "First Public Beta Release"
date: 2020-06-11T7:27:23+01:00
tags: ["release"]
draft: false
---

We are happy to release the first public beta version of the tool.

Even if we know that many things are missing and bugs are obviously existing in this beta, we would like to let you know about the tool and the way it works.

### Goal

Use LabVIEW content to generate valuable documentation that wil help maintain your projects and onboard new resources in the development team.

When we say "LabVIEW content" we not only speak about vi description, we also speak about how the different part of the code are linked together.

As a DQMH user, the tool has been primarily developed to get most information from this framework. We hope to support OPP and other frameworks in the future.

### How to

You can generate the documentation of your LabVIEW project running the tool from Tools>>Wovalab>>AntiDoc>>Generate Project Documentation... menu item.

![Antidoc LabVIEW menu](https://wovalab.gitlab.io/open-source/labview-doc-generator/img/AntiDoc-tool-menu.png)

After minimal configuration (1-Select the lvproj you want to document ; 2-Fill in document title, author name and e-mail address), you just have to hit the Generate documentation button (3).

![Antidoc tool user interface](https://wovalab.gitlab.io/open-source/labview-doc-generator/img/AntiDoc-tool-ui.png)

<!--more-->

An output folder is generated next to your *.lvproj file

![Antidoc output folder](https://wovalab.gitlab.io/open-source/labview-doc-generator/img/AntiDoc-output.png)

This folder contains Project-Documentation.adoc file and Images folder.

Project-Documentation.adoc is a plain text file using [Asciidoctor](https://asciidoctor.org/) syntax. Even if you can read this document as it, it remains not really nice to show to other people.
![Antidoc raw output file](https://wovalab.gitlab.io/open-source/labview-doc-generator/img/AntiDoc-asciidoc-syntax-file.png)

There are many ways to render this file. Among them, you can :

* open Project-Documentation.adoc file in Chrome with [Asciidoctor.js Live Preview](https://chrome.google.com/webstore/detail/asciidoctorjs-live-previe/iaalpfgpbocpdfblpnhhgllgbdbchmia) extension (don't forget to allow access to file URLs in the extension page to render the file).

* If you are using Gitlab to store you LabVIEW projects you can directly view it in your browser and it will be directly rendered without any extra extension. See an example [here](https://gitlab.com/wovalab/labview-project-documentation-with-antidoc-demo/-/blob/develop/DQMH-Project/Doc-Generator-Output/Project-Documentation.adoc)

* You also render the same adoc file to obtain a beautiful PDF using Asciidoctor toolchain

![Antidoc PDF file](https://wovalab.gitlab.io/open-source/labview-doc-generator/img/Antidoc-pdf-rendering.png)


Download the PDF file: [Project-Documentation.pdf](https://wovalab.gitlab.io/open-source/labview-doc-generator/doc/Project-Documentation.pdf)


### Download
[AntiDoc - Beta - v1.0.14](https://wovalab.gitlab.io/open-source/labview-doc-generator/vip/wovalab_lib_antidoc-1.0.0.14.vip)


### Installation

Installation can be made with the free version of [VI Package Manager](https://vipm.jki.net/get).

### LabVIEW supported version

2014 (32 and 64 bit) and above